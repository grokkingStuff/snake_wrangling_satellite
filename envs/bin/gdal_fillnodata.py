#!/Users/grokkingstuff/Sync/work/snake_wrangling_satellite/envs/bin/python

import sys

from osgeo.gdal import deprecation_warn

# import osgeo_utils.gdal_fillnodata as a convenience to use as a script
from osgeo_utils.gdal_fillnodata import *  # noqa
from osgeo_utils.gdal_fillnodata import main

deprecation_warn("gdal_fillnodata")
sys.exit(main(sys.argv))
